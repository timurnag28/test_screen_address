import React, {Component} from 'react';
import {Text, View, StyleSheet, Keyboard, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {MontserratBold, MontserratRegular} from '../share/fonts';
import {InputView} from '../share/components/InputView';
import store from '../stores';
import {observer} from 'mobx-react';
import {WINDOW_WIDTH} from '../share/consts';
import {ActionButton} from '../share/components/ActionButton';

@observer
export class EditAddressScreen extends Component {
  render() {
    const {
      address,
      flat,
      floor,
      house,
      porch,
      setAddress,
      setFlat,
      setFloor,
      setHouse,
      setPorch,
    } = store.addressStore;

    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={Keyboard.dismiss}
        style={{flex: 1, paddingHorizontal: 16}}>
        <View
          style={{flexDirection: 'row', paddingTop: 14, alignItems: 'center'}}>
          <Ionicons
            onPress={() => {
              alert('возврат назад'), Keyboard.dismiss();
            }}
            name={'md-arrow-back'}
            size={25}
          />
          <Text
            style={{paddingLeft: 72, fontSize: 18, fontFamily: MontserratBold}}>
            Изменить адрес
          </Text>
        </View>
        <View style={{paddingTop: 16}}>
          <Text style={styles.headTitle}>Улица</Text>
          <InputView
            onSubmitEditing={() => {
              this.secondTextInput.focus();
            }}
            blurOnSubmit={false}
            value={address}
            onChangeText={setAddress}
          />
        </View>
        <View
          style={{
            paddingTop: 16,
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'space-between',
          }}>
          <View>
            <Text style={styles.headTitle}>Дом</Text>
            <InputView
              forwardedRef={(input) => {
                this.secondTextInput = input;
              }}
              blurOnSubmit={false}
              onSubmitEditing={() => {
                this.thirdTextInput.focus();
              }}
              maxLength={4}
              keyboardType={'phone-pad'}
              style={styles.input}
              value={house}
              onChangeText={setHouse}
            />
          </View>
          <View>
            <Text style={styles.headTitle}>Подъезд</Text>
            <InputView
              forwardedRef={(input) => {
                this.thirdTextInput = input;
              }}
              blurOnSubmit={false}
              onSubmitEditing={() => {
                this.fourthTextInput.focus();
              }}
              maxLength={2}
              keyboardType={'phone-pad'}
              style={styles.input}
              value={porch}
              onChangeText={setPorch}
            />
          </View>
          <View>
            <Text style={styles.headTitle}>Квартира</Text>
            <InputView
              forwardedRef={(input) => {
                this.fourthTextInput = input;
              }}
              blurOnSubmit={false}
              onSubmitEditing={() => {
                this.fifthTextInput.focus();
              }}
              maxLength={4}
              keyboardType={'phone-pad'}
              style={styles.input}
              value={flat}
              onChangeText={setFlat}
            />
          </View>
          <View>
            <Text style={styles.headTitle}>Этаж</Text>
            <InputView
              forwardedRef={(input) => {
                this.fifthTextInput = input;
              }}
              onSubmitEditing={Keyboard.dismiss}
              maxLength={2}
              keyboardType={'phone-pad'}
              style={styles.input}
              value={floor}
              onChangeText={setFloor}
            />
          </View>
        </View>
        <ActionButton
          style={{marginBottom: 16}}
          text={'Сохранить и выйти'}
          onPress={() => {
            alert('сохранение и выход'), Keyboard.dismiss();
          }}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  headTitle: {
    color: 'rgba(0, 0, 0, 0.4)',
    fontFamily: MontserratRegular,
    fontSize: 13,
  },
  input: {width: WINDOW_WIDTH / 4.5},
});
