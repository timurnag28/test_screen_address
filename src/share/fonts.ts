export const MontserratBold = 'MontserratBold';
export const MontserratRegular = 'MontserratRegular';
export const MontserratMedium = 'MontserratMedium';
export const MontserratSemiBold = 'MontserratSemiBold';
