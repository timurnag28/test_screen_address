import {
  StyleProp,
  Text,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';
import React from 'react';
import {MontserratBold} from '../fonts';

export const ActionButton = ({
  onPress,
  text,
  style,
}: {
  onPress: () => void;
  text: string;
  style?: StyleProp<ViewStyle>;
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        {
          paddingVertical: 7,
          backgroundColor: '#FF6501',
          alignItems: 'center',
          borderRadius: 30,
          marginTop: 10,
        },
        style,
      ]}>
      <Text
        style={{
          color: '#FFFFFF',
          fontSize: 18,
          fontFamily: MontserratBold,
        }}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};
