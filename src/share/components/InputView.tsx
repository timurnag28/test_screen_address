import {
  KeyboardTypeOptions,
  StyleProp,
  TextInput,
  TextStyle,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {MontserratRegular} from '../fonts';

export const InputView = ({
  value,
  onChangeText,
  placeholder,
  maxLength,
  style,
  keyboardType,
  onSubmitEditing,
  blurOnSubmit,
  forwardedRef,
}: {
  value: string;
  onChangeText: (text: string) => void;
  placeholder?: string;
  maxLength?: number;
  style?: StyleProp<TextStyle>;
  keyboardType?: KeyboardTypeOptions;
  onSubmitEditing?: () => void;
  blurOnSubmit?: boolean;
  forwardedRef?: any;
}) => {
  const [borderColor, setBorderColor] = useState('rgba(0, 0, 0, 0.2)');
  return (
    <View style={style}>
      <TextInput
        ref={forwardedRef}
        blurOnSubmit={blurOnSubmit}
        onSubmitEditing={onSubmitEditing}
        onBlur={() => setBorderColor('rgba(0, 0, 0, 0.2)')}
        onFocus={() => setBorderColor('#FF6501')}
        underlineColorAndroid={borderColor}
        maxLength={maxLength}
        style={[
          {fontFamily: MontserratRegular, fontSize: 14, color: '#000000'},
        ]}
        keyboardType={keyboardType}
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
      />
    </View>
  );
};

export default React.forwardRef((props, ref) => (
  <InputView forwardedRef={ref} {...props} />
));
