import {action, observable} from 'mobx';

class AddressStore {
  @observable address: string = '';
  @observable house: string = '';
  @observable porch: string = '';
  @observable flat: string = '';
  @observable floor: string = '';

  @action
  setAddress = (text: string) => {
    this.address = text;
  };
  @action
  setHouse = (text: string) => {
    this.house = text;
  };
  @action
  setPorch = (text: string) => {
    this.porch = text;
  };
  @action
  setFlat = (text: string) => {
    this.flat = text;
  };
  @action
  setFloor = (text: string) => {
    this.floor = text;
  };
}

const addressStore = new AddressStore();
export default addressStore;
