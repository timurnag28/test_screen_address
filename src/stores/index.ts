import addressStore from './AddressStore';

const store = {
  addressStore,
};

export default store;
